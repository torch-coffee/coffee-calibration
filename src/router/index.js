import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home'
import Welcome from '../views/Welcome'
import Scan from '../views/Scan'
import Cupping from '../views/Cupping'

const routes = [
  {
    path: '/home',
    name: 'Home',
    component: Home,
  },
  {
    path: '/',
    name: 'Welcome',
    component: Welcome,
    // TODO: Add auth guard here - route to home if authed
  },
  {
    path: '/scan',
    name: 'Scan',
    component: Scan,
    // TODO: Add auth guard here - route to home if authed
  },
  {
    path: '/cupping',
    name: 'Cupping',
    component: Cupping,
    // TODO: Add auth guard here - route to home if authed
  },
  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () =>
  //     import(/* webpackChunkName: "about" */ '../views/About.vue'),
  // },
]

export default createRouter({
  history: createWebHistory(),
  base: process.env.BASE_URL,
  routes,
})
